package com.tdd.bootcamp2;

import java.util.Observable;
import java.util.Observer;

public class Player implements Observer {

    private int scoreKeeper;
    private MovableBehaviour movableBehaviour;

    public Player(MovableBehaviour movableBehaviour) {
        this.movableBehaviour = movableBehaviour;
        this.scoreKeeper = 0;
    }

    public void addScore(int score) {
        this.scoreKeeper += score;
    }

    public MoveEnum getLatestMove() {
        return movableBehaviour.nextMove();
    }

    public int score() {
        return scoreKeeper;
    }

    @Override
    public void update(Observable game, Object opponentMove) {
        ((CopyCatBehaviour) this.movableBehaviour).setOpponentMove((MoveEnum) opponentMove);
    }

    public MovableBehaviour getMovableBehaviour() {
        return this.movableBehaviour;
    }
}
