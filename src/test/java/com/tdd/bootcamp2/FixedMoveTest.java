package com.tdd.bootcamp2;

import org.junit.Assert;
import org.junit.Test;

public class FixedMoveTest {

    @Test
    public void shouldReturnCooperateMove () {
        Player cooperatePlayer = new Player(MovableBehaviour.COOPERATE_BEHAVIOUR);
        Assert.assertEquals(MoveEnum.COOPERATE, cooperatePlayer.getLatestMove());
    }

    @Test
    public void shouldReturnCheatMove () {
        Player cooperatePlayer = new Player(MovableBehaviour.CHEAT_BEHAVIOUR);
        Assert.assertEquals(MoveEnum.CHEAT, cooperatePlayer.getLatestMove());
    }
}
