package com.tdd.bootcamp2;

import com.tdd.bootcamp2.Machine;
import com.tdd.bootcamp2.MoveEnum;
import org.junit.Assert;
import org.junit.Test;


public class MachineTest {
    @Test
    public void shouldReturnTwoPointsWhenBothPlayersCooperate() {
        Machine machine = new Machine();
        int[] expectedArray = new int[]{2, 2};
        Assert.assertArrayEquals(expectedArray, machine.calculateScore(MoveEnum.COOPERATE, MoveEnum.COOPERATE));
    }

    @Test
    public void shouldReturnZeroPointsWhenBothPlayersCheat() {
        Machine machine = new Machine();
        int[] expectedArray = new int[]{0, 0};
        Assert.assertArrayEquals(expectedArray, machine.calculateScore(MoveEnum.CHEAT, MoveEnum.CHEAT));
    }

    @Test
    public void shouldReturnMinusOneAndThreeWhenPlayer1CooperateAndPlayer2Cheat() {
        Machine machine = new Machine();
        int[] expectedArray = new int[]{-1, 3};
        Assert.assertArrayEquals(expectedArray, machine.calculateScore(MoveEnum.COOPERATE, MoveEnum.CHEAT));
    }

    @Test
    public void shouldReturnThreeAndMinusOneWhenPlayer1CheatAndPlayer2Cooperate() {
        Machine machine = new Machine();
        int[] expectedArray = new int[]{3, -1};
        Assert.assertArrayEquals(expectedArray, machine.calculateScore(MoveEnum.CHEAT, MoveEnum.COOPERATE));
    }
}