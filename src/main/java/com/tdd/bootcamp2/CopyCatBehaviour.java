package com.tdd.bootcamp2;

public class CopyCatBehaviour implements MovableBehaviour {

    private MoveEnum opponentMove = MoveEnum.COOPERATE;

    @Override
    public MoveEnum nextMove() {
        return opponentMove;
    }

    public void setOpponentMove(MoveEnum opponentMove) {
        if(opponentMove != null) {
            this.opponentMove = opponentMove;
        }
    }
}
