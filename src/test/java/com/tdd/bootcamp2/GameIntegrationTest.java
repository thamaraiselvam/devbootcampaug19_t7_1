package com.tdd.bootcamp2;

import org.junit.Ignore;
import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class GameIntegrationTest {

    @Test
    public void shouldPlayGameBetweenCheatPlayerAndCooperatePlayer() {
        PrintStream console = mock(PrintStream.class);
        Player cheatPlayer = new Player(MovableBehaviour.CHEAT_BEHAVIOUR);
        Player cooperatePlayer = new Player(MovableBehaviour.COOPERATE_BEHAVIOUR);
        Game game = new Game(cheatPlayer, cooperatePlayer, console, 5, new Machine());
        game.play();
        verify(console).println("Score is 3 -1");
        verify(console).println("Score is 6 -2");
        verify(console).println("Score is 9 -3");
        verify(console).println("Score is 12 -4");
        verify(console).println("Score is 15 -5");
    }

    @Test
    public void shouldPlayGameBetweenCopyCatPlayerAndCooperatePlayer() {
        PrintStream console = mock(PrintStream.class);
        Player copyCatPlayer = new Player(new CopyCatBehaviour());
        Player cooperatePlayer = new Player(MovableBehaviour.COOPERATE_BEHAVIOUR);
        Game game = new Game(copyCatPlayer, cooperatePlayer, console, 5, new Machine());
        game.addObserver(copyCatPlayer);
        game.play();
        verify(console).println("Score is 2 2");
        verify(console).println("Score is 4 4");
        verify(console).println("Score is 6 6");
        verify(console).println("Score is 8 8");
        verify(console).println("Score is 10 10");
    }

    @Test
    public void shouldPlayGameBetweenCopyCatPlayerAndCheatPlayer() {
        PrintStream console = mock(PrintStream.class);
        Player copyCatPlayer = new Player(new CopyCatBehaviour());
        Player cheatPlayer = new Player(MovableBehaviour.CHEAT_BEHAVIOUR);
        Game game = new Game(copyCatPlayer, cheatPlayer, console, 5, new Machine());
        game.addObserver(copyCatPlayer);
        game.play();
        verify(console,times(5)).println("Score is -1 3");
    }

    @Test
    public void shouldPlayGameBetweenCopyCatPlayerAndCopyCatPlayer() {
        PrintStream console = mock(PrintStream.class);
        Player copyCatPlayer1 = new Player(new CopyCatBehaviour());
        Player copyCatPlayer2 = new Player(new CopyCatBehaviour());
        Game game = new Game(copyCatPlayer1, copyCatPlayer2, console, 5, new Machine());
        game.addObserver(copyCatPlayer1);
        game.addObserver(copyCatPlayer2);
        game.play();
        verify(console).println("Score is 2 2");
        verify(console).println("Score is 4 4");
        verify(console).println("Score is 6 6");
        verify(console).println("Score is 8 8");
        verify(console).println("Score is 10 10");
    }

    @Test
    public void shouldPlayGameBetweenCooperatePlayerAndCopyCatPlayer() {
        PrintStream console = mock(PrintStream.class);
        Player cooperatePlayer = new Player(MovableBehaviour.COOPERATE_BEHAVIOUR);
        Player copyCatPlayer = new Player(new CopyCatBehaviour());
        Game game = new Game(cooperatePlayer, copyCatPlayer, console, 5, new Machine());
        game.addObserver(copyCatPlayer);
        game.play();
        verify(console).println("Score is 2 2");
        verify(console).println("Score is 4 4");
        verify(console).println("Score is 6 6");
        verify(console).println("Score is 8 8");
        verify(console).println("Score is 10 10");
    }

    @Test
    public void shouldPlayGameBetweenCheatPlayerAndCopyCatPlayer() {
        PrintStream console = mock(PrintStream.class);
        Player cheatPlayer = new Player(MovableBehaviour.CHEAT_BEHAVIOUR);
        Player copyCatPlayer = new Player(new CopyCatBehaviour());
        Game game = new Game(cheatPlayer, copyCatPlayer, console, 5, new Machine());
        game.addObserver(copyCatPlayer);
        game.play();
        verify(console,times(5)).println("Score is 3 -1");
    }
}
